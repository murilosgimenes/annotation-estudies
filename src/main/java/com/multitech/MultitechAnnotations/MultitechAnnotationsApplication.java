package com.multitech.MultitechAnnotations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultitechAnnotationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultitechAnnotationsApplication.class, args);
	}

}
